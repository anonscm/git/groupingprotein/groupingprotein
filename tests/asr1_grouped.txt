>GST-sans-ASR1
GST-sans-ASR1
a1.a1.a1
>Oliv1GST-sans-ASR1
Oliv1GST-sans-ASR1
a1.a1.a2
>Oliv2GST-sans-ASR1
Oliv2GST-sans-ASR1
a1.a1.a3
>ASR1-clivee
ASR1-clivee
a1.a2.a1
>gi|27807057|ref|NP_777009.1|
gi|27807057|ref|NP_777009.1| cAMP-dependent protein kinase catalytic subunit alpha [Bos taurus]MGNAAAAKKGSEQESVKEFLAKAKEDFLKKWENPAQNTAHLDQFERIKTLGTGSFGRVML
a2.a1.a1
>sp|P17948|VGFR1_HUMAN
sp|P17948|VGFR1_HUMAN Vascular endothelial growth factor receptor 1 OS=Homo sapiens GN=FLT1 PE=1 SV=2
a3.a1.a1
>sp|Q940H6|SRK2E_ARATH
sp|Q940H6|SRK2E_ARATH Serine/threonine-protein kinase SRK2E OS=Arabidopsis thaliana GN=SRK2E PE=1 SV=1
a4.a1.a1

