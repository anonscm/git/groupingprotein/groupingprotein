# Loading and parsing xml files
gp-read-xtandem.py /rep1/*.xml /rep2/*.xml /repn*.xml -e 0.01 | bzip2 -z -c > file.bz2

# grouping
bzip2 -d -c file.bz2 | gp-grouping --peprepro 2 --pepbyprot 2 --fasta /database/contaminants_standarts.fasta | bzip2 -c -z > file_group.bz2

# export proteins list
cat file_group.bz2 | bzip2 -c -d | gp-output-protein-list.py > list_tot_prot.tsv

# export peptides list
cat file_group.bz2 | bzip2 -c -d | gp-output-peptide-list.py > list_tot_peptide.tsv

# export counts proteins
cat file_group.bz2 | bzip2 -c -d | gp-output-protein-counting.py > count_prot.tsv

# export counts peptides
cat file_group.bz2 | bzip2 -c -d | gp-output-peptide-counting.py > count_peptides.tsv

# export masschroqML file
cat file_group.bz2 | bzip2 -c -d | gp-output-masschroqml.py > file.masschroqML

