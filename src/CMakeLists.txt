# cpp module src
#compile gp_grouping program
add_subdirectory (cpp)

# python module src
#configure and copie python file
add_subdirectory (python)

#INSTALL python private library xmlstreamwriter
FILE(GLOB pythonlib  "${CMAKE_CURRENT_SOURCE_DIR}/python/xmlstreamwriter/*.py")
INSTALL(FILES ${pythonlib} DESTINATION lib/groupingprotein/xmlstreamwriter)

#INSTALL python script
FILE(GLOB pythonbin  "${CMAKE_CURRENT_BINARY_DIR}/python/*.py")
INSTALL(PROGRAMS ${pythonbin} DESTINATION bin)
