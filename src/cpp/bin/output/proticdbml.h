/**
 * \file output/proticdbml.h
 * \date 11/5/2017
 * \author Olivier Langella
 * \brief PROTICdbML writer
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QXmlStreamWriter>
#include <QFile>
#include <QString>
#include <QTime>
#include <pappsomspp/amino_acid/aamodification.h>
#include <pappsomspp/protein/protein.h>
#include "../input/xtandemsaxhandler.h"
#include <set>

struct ProticPeptideHit;


struct ProticPeptideHitRef
{
  // QString peptide_hit_id;
  QString seq_id;
  unsigned int start;
  unsigned int stop;
  QString residue_before_nter;
  QString residue_after_cter;
};

class ProticdbMl
{
  public:
  ProticdbMl(const QString &out_filename);
  ~ProticdbMl();

  void writeHeader();
  void writeSequence(const QString &id,
                     const QString &access,
                     const QString &desc,
                     const QString &seq);
  void writeSample(const QString &sample_id, const QString &sample_name);
  void writeMsRun(const QString &msrun_id,
                  const QString &sample_id,
                  const QString &sample_name,
                  const QString &filename);
  void writeIdentificationRunDesccriptionDataProcessingList(
    std::map<QString, QString> m_map_sampleid2filepath);
  void closeSequences();
  void closeSamples();
  void closeMsRuns();
  void closeMatchs();
  void closePeptideHits();
  void writePeptideHit(const std::set<QString> &accession_set,
                       const QString &grouping_id,
                       const QStringList &sg_list,
                       const QString &sample_id,
                       const QString &seqli,
                       unsigned int scan,
                       unsigned int charge);

  void writeMatch(const QString &subgroup_id, const QStringList &protein_ids);
  void close();

  private:
  std::vector<PeptideHit>
  findXtandemPeptideHit(const std::set<QString> &accession_set,
                        const QString &sample_id,
                        const QString &seqli,
                        unsigned int scan,
                        unsigned int charge);
  void writeOboModif(pappso::AaModificationP mod);
  void writeCvParam(QString acc, QString value, QString description);
  // void writeProject();
  void writeIdentMethod();
  // void writeSamples();
  // void writeMsRuns();
  // void writeIdentificationRun(IdentificationGroup *p_identification);
  // void writeIdentificationDataSource();
  //  IdentificationDataSource *p_identification_data_source);
  // void writepeptideHits(IdentificationGroup *p_identification);
  // void writepeptideHitsbyGroup(GroupingGroup *p_group);

  void writePtms(pappso::Peptide *p_peptide);
  // void writeMatchs(IdentificationGroup *p_identification);
  // void writeMatch(std::vector<const ProteinMatch *> &protein_match_sg_list);

  // const QString &getPeptideXmlId(const PeptideMatch *p_peptide_match) const;
  // const QString &getProteinXmlId(const ProteinMatch *p_protein_match) const;

  private:
  QFile *_output_file;
  QXmlStreamWriter *_output_stream;
  // ProjectSp _sp_project;
  // IdentificationGroup *_p_identification_group;
  QTime _duracel;
  std::map<QString, QString> _map_accession2xmlid;
  std::map<QString, double> _map_protid_mw;
  std::map<QString, QString> m_sampleid_name;
  std::map<QString, QString> _peptidekey_to_id;

  std::map<QString, XtandemSaxHandler *> _map_sampleid_data;
  std::map<QString, std::set<QString>> m_map_sg_sampleid;
  std::map<QString, std::vector<QString>> m_map_sg_phid;

  std::map<QString, std::vector<ProticPeptideHitRef>> m_map_phid_phref;

  unsigned int m_count_peptide = 0;
};
