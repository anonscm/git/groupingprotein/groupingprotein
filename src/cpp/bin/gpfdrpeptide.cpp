/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of Protein Grouper.
 *
 *     Protein Grouper is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Protein Grouper is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Protein Grouper.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <QString>
#include <QLocale>
#include <QDateTime>
#include <QDir>
#include <QTimer>
#include <QCommandLineParser>
#include <iostream>
#include <pappsomspp/pappsoexception.h>
#include "gp_lib_config.h"
#include "gpfdrpeptide.h"
#include "../libgroupingprotein/gp_error.h"
#include "input/peptideresultsqvaluehandler.h"


GpFdrPeptide::GpFdrPeptide(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
GpFdrPeptide::run()
{
  // ./src/cpp/bin/gp-output-proticdbml -i
  // '/gorgone/pappso/versions_logiciels_pappso/gp/groups.xml' -f
  // /gorgone/pappso/moulon/users/thierry/amaizing/reinterrogation_xtandem_point_mut_isotopes_271115/protic/amaizing_database.fasta
  // -o /gorgone/pappso/versions_logiciels_pappso/gp/protic.xml


  //./src/pt-fastarenamer -i
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_ortho_uniprot_TK24_M145.fasta
  //-c
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_TK24_M145.ods
  //-o /tmp/test.fasta

  QTextStream errorStream(stderr, QIODevice::WriteOnly);
  QTextStream outStream(stdout, QIODevice::WriteOnly);

  try
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(
        QString("gp-fdr-filter")
          .append(" ")
          .append(GP_VERSION)
          .append(" compute Evalue threshold given a target FDR"));
      parser.addHelpOption();
      parser.addVersionOption();
      QCommandLineOption inputOption(
        QStringList() << "i"
                      << "peptides",
        QCoreApplication::translate(
          "main", "XML peptide identification result file <peptides>."),
        QCoreApplication::translate("main", "input"));

      QCommandLineOption decoyOption(
        QStringList() << "decoy-regexp",
        QCoreApplication::translate(
          "main",
          "regular expression to select decoy protein accessions and remove "
          "them "
          "before grouping. default : .*\\|reversed$"),
        QCoreApplication::translate("main", "string"),
        QString());

      parser.addOption(inputOption);
      parser.addOption(decoyOption);

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QStringList args   = parser.positionalArguments();


      QString peptidesFileStr = parser.value(inputOption);
      QString decoyOptionStr  = parser.value(decoyOption);


      PeptideResultsQvalueHandler parser_peptide;

      if(!decoyOptionStr.isEmpty())
        {
          parser_peptide.setDecoyRegexp(decoyOptionStr);
        }
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      QXmlSimpleReader simplereader;
      simplereader.setContentHandler(&parser_peptide);
      simplereader.setErrorHandler(&parser_peptide);

      QFile peptide_file;
      if(peptidesFileStr.isEmpty())
        {
          peptide_file.open(stdin, QIODevice::ReadOnly);
        }
      else
        {
          peptide_file.setFileName(peptidesFileStr);
          peptide_file.open(QIODevice::ReadOnly);
        }

      QXmlInputSource xmlInputSource(&peptide_file);
      if(simplereader.parse(xmlInputSource))
        {
        }
      else
        {
          throw GpError(parser_peptide.errorString());
        }


      outStream << "target psms\t"
                << QString::number(parser_peptide.getCountTarget(), 'g', 6)
                << "\n";
      outStream << "decoy psms\t"
                << QString::number(parser_peptide.getCountDecoy(), 'g', 6)
                << "\n";

      double fdr = ((double)parser_peptide.getCountDecoy() /
                    (double)parser_peptide.getCountTarget());
      outStream << "FDR\t" << QString::number(fdr, 'g', 6) << "\t("
                << QString::number(fdr * 100.0, 'g', 6) << "%)\n";


      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
    }
  catch(GpError &gperror)
    {
      errorStream << "Oops! an error occurred in GP. Dont Panic :" << endl;
      errorStream << gperror.qwhat() << endl;
      exit(1);
      app->exit(1);
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in GP. Dont Panic :" << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in GP. Dont Panic :" << endl;
      errorStream << error.what() << endl;
      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
GpFdrPeptide::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
GpFdrPeptide::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QCoreApplication app(argc, argv);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QCoreApplication::setApplicationName("gp-fdr-peptide");
  QCoreApplication::setApplicationVersion(GP_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  GpFdrPeptide myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
