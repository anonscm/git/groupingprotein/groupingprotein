/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of Protein Grouper.
 *
 *     Protein Grouper is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Protein Grouper is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Protein Grouper.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "groupinghandler.h"
#include "../output/proticdbml.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/fasta/fastareader.h>
#include <pappsomspp/utils.h>

#include <QDebug>


class FastaSequenceHandler : public pappso::FastaHandlerInterface
{
  public:
  FastaSequenceHandler(std::map<QString, QString> *m_map_sequences)
    : mp_map_sequences(m_map_sequences){};

  void
  setSequence(const QString &description, const QString &sequence) override
  {
    pappso::Protein protein(description, sequence);
    mp_map_sequences->insert(std::pair<QString, QString>(
      protein.getAccession(), protein.removeTranslationStop().getSequence()));
  };

  private:
  std::map<QString, QString> *mp_map_sequences;
};


GroupingHandler::GroupingHandler(ProticdbMl *writer,
                                 const QString &fasta_file_str)
  : p_writer(writer)
{
  p_writer->writeHeader();


  QFile fastaFile;
  fastaFile.setFileName(fasta_file_str);
  fastaFile.open(QIODevice::ReadOnly);
  FastaSequenceHandler sequences(&m_map_sequences);
  pappso::FastaReader reader(sequences);
  reader.parse(&fastaFile);
  fastaFile.close();
}

GroupingHandler::~GroupingHandler()
{
}

bool
GroupingHandler::startElement(const QString &namespaceURI,
                              const QString &localName,
                              const QString &qName,
                              const QXmlAttributes &attributes)
{
  try
    {
      if(_firstElement)
        {
          _firstElement = false;
          if(qName != "grouping_protein")
            throw new pappso::PappsoException(
              QString("The input is not a grouping result file"));
        }
      else if(qName == "sample")
        {
          m_map_sampleid2name.insert(std::pair<QString, QString>(
            attributes.value("id"), attributes.value("name")));
          m_current_id = attributes.value("id");
        }
      else if(qName == "xtandem")
        {
          m_map_sampleid2filepath.insert(std::pair<QString, QString>(
            m_current_id, attributes.value("file")));
        }
      else if(qName == "protein")
        {
          // GRMZM2G132090_P01|reversed
          // GRMZM2G010791_P01
          /*ERROR in GroupingHandler::startElement tag spectrum, PAPPSO
exception: _map_accession2xmlid[GRMZM2G010791_P01] not found
*/
          QString protein_accession = attributes.value("access");
          QString protein_sequence  = m_map_sequences[protein_accession];
          if(protein_accession.endsWith("|reversed"))
            {
              protein_sequence =
                m_map_sequences[protein_accession.remove("|reversed")];

              std::string reverse = protein_sequence.toStdString();
              std::reverse(reverse.begin(), reverse.end());
              protein_sequence = QString::fromStdString(reverse);
            }
          if(protein_sequence.isEmpty())
            {
              throw pappso::PappsoException(
                QObject::tr("m_map_sequences[%1] not found")
                  .arg(attributes.value("access")));
            }
          p_writer->writeSequence(attributes.value("id"),
                                  attributes.value("access"),
                                  attributes.value("desc"),
                                  protein_sequence);
          m_map_protein_xmlid_accessions.insert(std::pair<QString, QString>(
            attributes.value("id"), attributes.value("access")));
        }
      else if(qName == "group")
        {
          m_current_group_accession_set.clear();
          m_current_group_number = attributes.value("number").toULong();
        }
      else if(qName == "subgroup")
        {
          //<subgroup id="a2.a1" number="1" protIds="a2.a1.a1"/>
          m_map_sg_proteins.insert(std::pair<QString, QStringList>(
            attributes.value("id"), attributes.value("protIds").split(" ")));
          for(auto xml_protein_id : attributes.value("protIds").split(" "))
            {
              QString protein_accession =
                m_map_protein_xmlid_accessions[xml_protein_id];
              if(protein_accession.isEmpty())
                {
                  throw pappso::PappsoException(
                    QObject::tr("m_map_protein_xmlid_accessions[%1] not found")
                      .arg(xml_protein_id));
                }
              m_current_group_accession_set.insert(protein_accession);
            }
        }
      else if(qName == "peptide")
        {
          //<peptide id="pepa2a1" seqLI="AAIVADEIAEQAEFFSFGTNDITQMTFGYSR"
          // mhTheo="3429.5944" subgroupIds="a2.a2 a2.a3">
          current_peptide_id = attributes.value("id");
          if (current_peptide_id.contains(".")) {
              current_peptide_id = current_peptide_id.replace(".", pappso::Utils::getLexicalOrderedString(m_current_group_number));
          }
          current_sg_id_list = attributes.value("subgroupIds").split(" ");
          current_mhtheo     = attributes.value("mhTheo").toDouble();
          current_seqli      = attributes.value("seqLI");
        }
      else if(qName == "spectrum")
        {
          //<spectrum sample="samp2" scan="14906" charge="3"/>
          p_writer->writePeptideHit(m_current_group_accession_set,
                                    current_peptide_id,
                                    current_sg_id_list,
                                    attributes.value("sample"),
                                    current_seqli,
                                    attributes.value("scan").toUInt(),
                                    attributes.value("charge").toUInt());
        }

      /*
    else if(qName == "mod")
      {
        if(attributes.value("pm").isEmpty())
          { // not a point mutation
            //<mod aa="C" pos="18" mod="57.04" />
            _currentModifs += attributes.value("aa") +
                              attributes.value("pos") + ":" +
                              attributes.value("mod") + " ";
            pappso::AaModificationP modif =
              pappso::AaModification::getInstanceCustomizedMod(
                attributes.value("mod").toDouble());
            _sp_current_peptide.get()->addAaModification(modif, 0);
          }
        else
          {
            _currentModifs += QString("pm@%1:%2=>%3 ")
                                .arg(attributes.value("pos"))
                                .arg(attributes.value("aa"))
                                .arg(attributes.value("pm"));
          }
      }
      */
    }
  catch(pappso::PappsoException &exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in GroupingHandler::startElement tag %1, PAPPSO "
                    "exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception &exception_std)
    {
      _errorStr = QObject::tr(
                    "ERROR in GroupingHandler::startElement tag %1, std "
                    "exception:\n%2")
                    .arg(qName)
                    .arg(exception_std.what());
      return false;
    }
  _currentText = "";
  return true;
}

bool
GroupingHandler::endElement(const QString &namespaceURI,
                            const QString &localName,
                            const QString &qName)
{
  try
    {

      if(qName == "protein_list")
        {
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          p_writer->closeSequences();
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          for(auto pair_sample : m_map_sampleid2name)
            {
              p_writer->writeSample(pair_sample.first, pair_sample.second);
            }
          p_writer->closeSamples();
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          for(auto pair_sample : m_map_sampleid2name)
            {
              p_writer->writeMsRun(QString("msr%1").arg(pair_sample.first),
                                   pair_sample.first,
                                   pair_sample.second,
                                   QString("%1.RAW").arg(pair_sample.second));
            }
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          p_writer->closeMsRuns();
          p_writer->writeIdentificationRunDesccriptionDataProcessingList(
            m_map_sampleid2filepath);
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
        }
    }
  catch(pappso::PappsoException &exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in GroupingHandler::startElement tag %1, PAPPSO "
                    "exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception &exception_std)
    {
      _errorStr = QObject::tr(
                    "ERROR in GroupingHandler::startElement tag %1, std "
                    "exception:\n%2")
                    .arg(qName)
                    .arg(exception_std.what());
      return false;
    }
  return true;
}

bool
GroupingHandler::endDocument()
{
  p_writer->closePeptideHits();
  // p_writer->setSgProteinsMap (m_map_sg_proteins);
  for(auto pair_sg : m_map_sg_proteins)
    {
      p_writer->writeMatch(pair_sg.first, pair_sg.second);
    }
  p_writer->closeMatchs();
  return true;
}

bool
GroupingHandler::startDocument()
{
  return true;
}

bool
GroupingHandler::characters(const QString &str)
{
  _currentText += str;
  return true;
}

bool
GroupingHandler::fatalError(const QXmlParseException &exception)
{
  _errorStr = QObject::tr(
                "Parse error at line %1, column %2:\n"
                "%3")
                .arg(exception.lineNumber())
                .arg(exception.columnNumber())
                .arg(exception.message());
  return false;
}

QString
GroupingHandler::errorString() const
{
  return _errorStr;
}

QString
GroupingHandler::getAccessionTodesc(QString &desc)
{
  return desc.split(" ").at(0);
}
