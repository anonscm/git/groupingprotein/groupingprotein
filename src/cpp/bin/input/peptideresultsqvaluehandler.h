/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of Protein Grouper.
 *
 *     Protein Grouper is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Protein Grouper is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Protein Grouper.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QXmlDefaultHandler>


struct PsmScore
{
  double m_evalue = 0;
  bool m_isDecoy  = false;
  double m_qvalue = 0;
};

class ProticdbMl;

class PeptideResultsQvalueHandler : public QXmlDefaultHandler
{
  public:
  PeptideResultsQvalueHandler();
  virtual ~PeptideResultsQvalueHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes) override;

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName) override;

  bool startDocument() override;

  bool endDocument() override;

  bool characters(const QString &str) override;

  bool fatalError(const QXmlParseException &exception) override;
  bool error(const QXmlParseException &exception) override;

  QString errorString() const override;

  double getEvalueThresholdForFdr(double target_fdr) const;

  void setDecoyRegexp(const QString &decoyOptionStr);

  std::size_t getCountDecoy() const;
  std::size_t getCountTarget() const;

  private:
  bool startElement_scan(QXmlAttributes attrs);
  bool startElement_psm(QXmlAttributes attributes);
  bool endElement_scan();

  private:
  std::vector<QString> _tag_stack;
  QString _errorStr;
  QString _current_text;


  std::vector<PsmScore> m_scoreList;
  std::map<QString, PsmScore> m_oneScanScoreList;
  QRegExp m_regexpDecoy;


  std::size_t m_countDecoy  = 0;
  std::size_t m_countTarget = 0;
};
