/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of Protein Grouper.
 *
 *     Protein Grouper is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Protein Grouper is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Protein Grouper.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <QXmlDefaultHandler>
#include <pappsomspp/peptide/peptide.h>
#include <set>

class ProticdbMl;

class GroupingHandler : public QXmlDefaultHandler
{
  public:
  GroupingHandler(ProticdbMl *writer, const QString &fasta_file_str);
  virtual ~GroupingHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);

  QString errorString() const;

  private:
  QString getAccessionTodesc(QString &);

  /// current parsed text
  QString _currentText;

  /// error message during parsing
  QString _errorStr;

  ProticdbMl *p_writer;

  bool _firstElement = true;

  QString m_current_id;
  std::map<QString, QString> m_map_sampleid2name;
  std::map<QString, QString> m_map_sampleid2filepath;
  std::map<QString, QString> m_map_sequences;
  std::map<QString, QString> m_map_protein_xmlid_accessions;

  std::map<QString, QStringList> m_map_sg_proteins;
  std::set<QString> m_current_group_accession_set;

  QString current_peptide_id;
  QStringList current_sg_id_list;
  double current_mhtheo;
  QString current_seqli;
  std::size_t m_current_group_number;
};
