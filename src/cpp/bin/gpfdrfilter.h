/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of Protein Grouper.
 *
 *     Protein Grouper is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Protein Grouper is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Protein Grouper.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>

#include <QObject>
#include <QCoreApplication>

/** @brief compiles psm results, compute qvalues for each PSM and gives an
 * Evalue threshold given the desired GpFdrFilter
 *
 * in memory structure needed to store each occurence of PSM evalue and if it is a decoy or target
 * sort it by Evalues
 * count target/decoy and rank it
 * find the level of Evalue corresponding to desired FDR
 *
 */
class GpFdrFilter : public QObject
{
  Q_OBJECT

  private:
  QCoreApplication *app;

  public:
  void windaube_exit();

  explicit GpFdrFilter(QObject *parent = 0);
  /////////////////////////////////////////////////////////////
  /// Call this to quit application
  /////////////////////////////////////////////////////////////
  void quit();

  signals:
  /////////////////////////////////////////////////////////////
  /// Signal to finish, this is connected to Application Quit
  /////////////////////////////////////////////////////////////
  void finished();

  public slots:
  /////////////////////////////////////////////////////////////
  /// This is the slot that gets called from main to start everything
  /// but, everthing is set up in the Constructor
  /////////////////////////////////////////////////////////////
  void run();

  /////////////////////////////////////////////////////////////
  /// slot that get signal when that application is about to quit
  /////////////////////////////////////////////////////////////
  void aboutToQuitApp();
};
