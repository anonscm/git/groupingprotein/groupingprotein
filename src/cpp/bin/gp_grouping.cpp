/*
 *
 * Protein Grouper
 * Copyright (C) 2014 Olivier Langella, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QString>
#include <QLocale>
#include <QDateTime>
#include <QDir>
#include <QTimer>
#include <iostream>
#include <pappsomspp/pappsoexception.h>
#include "gp_lib_config.h"
#include "gp_engine.h"
#include "gp_params.h"
#include "gp_error.h"
#include "gp_engine.h"
#include "gp_grouping.h"

using namespace std;


GroupingMain::GroupingMain(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();


  // connect up the signals
  QObject::connect(this, &GroupingMain::finished, app, &QCoreApplication::quit);
  QObject::connect(
    app, &QCoreApplication::aboutToQuit, this, &GroupingMain::aboutToQuitApp);
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
GroupingMain::run()
{
  QTextStream error_stream(stderr, QIODevice::WriteOnly);
  try
    {
      qDebug() << "GroupingMain::run() begin";

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "GroupingMain.Run is executing";


      const QDateTime dt_begin = QDateTime::currentDateTime();

      GpEngine *engine;
      GpParams *params;
      params = new GpParams(app);

      engine = new GpEngine(params);
      engine->performedTraitment();
      delete(params);
      delete(engine);
    }
  catch(const GpError &error)
    {
      error_stream << "Oops! an error occurred in gp-grouping. Dont Panic :"
                   << endl;
      error_stream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }
  catch(const pappso::PappsoException &error)
    {
      error_stream << "Oops! an error occurred in gp-grouping. Dont Panic :"
                   << endl;
      error_stream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }

  catch(const std::exception &error)
    {
      error_stream << "Oops! an error occurred in gp-grouping. Dont Panic :"
                   << endl;
      error_stream << error.what() << endl;
      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
GroupingMain::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
GroupingMain::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{

  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout);

  QCoreApplication app(argc, argv);
  QCoreApplication::setApplicationName("gp-grouping");
  QCoreApplication::setApplicationVersion(GP_VERSION);
  QLocale::setDefault(QLocale::system());


  // create the main class
  GroupingMain grouping_main;
  qDebug() << "main 2";


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &grouping_main, &GroupingMain::run);
  return app.exec();
}
