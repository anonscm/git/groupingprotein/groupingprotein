/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef GP_ERROR_H_
#define GP_ERROR_H_ 1

#include <exception>
#include <string>
#include <QString>

class GpError : public std::exception
{
  public:
  GpError(QString message) throw()
  {
    _message = message;
  }

  /*mcqError(std::string message = "") throw () :
   _message(message.c_str()) {
   }*/

  virtual const QString &
  qwhat() const throw()
  {
    return _message;
  }

  virtual const char *
  what() const throw()
  {
    return _message.toStdString().c_str();
  }

  virtual ~GpError() throw()
  {
  }

  private:
  QString _message; // Description de l'erreur
};

#endif /*GP_ERROR_H_*/
