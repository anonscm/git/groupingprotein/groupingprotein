/*
 * gp_params.cpp
 *
 *  Created on: 7 mars 2013
 *      Author: valot
 */

#include "gp_params.h"
#include <iostream>
#include <cstdio>
#include <QFile>
#include <QDebug>
#include <QFileInfo>
#include <QCommandLineParser>
#include "../libgroupingprotein/gp_lib_config.h"

using namespace std;

GpParams::GpParams(QCoreApplication *p_app) : _p_infile(NULL), _p_fasta(NULL)
{
  qDebug() << "GpParams::GpParams begin";
  _p_outfile = new QFile();
  _p_outfile->open(stdout, QIODevice::WriteOnly);
  _p_in      = new QTextStream(stdin, QIODevice::ReadOnly);
  _pepByProt = 2;
  _pepRepro  = 1;
  _decoy_regexp.setPattern(".*\\|reversed$");

  QCommandLineParser parser;
  // const QStringList args = parser.positionalArguments();
  // this->parseArguments(args);

  // throw pappso::PappsoException("test");
  parser.setApplicationDescription(QString("gp-grouping")
                                     .append(" ")
                                     .append(GP_VERSION)
                                     .append(" protein grouping algorithm."));
  parser.addHelpOption();
  parser.addVersionOption();

  QCommandLineOption inputOption(
    QStringList() << "i"
                  << "infile",
    QCoreApplication::translate("main", "Open peptide result default:STDIN"),
    QCoreApplication::translate("main", "input"));
  QCommandLineOption outputOption(
    QStringList() << "o"
                  << "outfile",
    QCoreApplication::translate("main", "Save grouping result default:STDOUT"),
    QCoreApplication::translate("main", "output"));

  QCommandLineOption pepbyprotOption(
    QStringList() << "pepbyprot",
    QCoreApplication::translate(
      "main",
      "Define the minimum number of peptide to conserve a protein. default:2"),
    QCoreApplication::translate("main", "int"),
    QString("2"));
  QCommandLineOption pepreproOption(
    QStringList() << "peprepro",
    QCoreApplication::translate("main",
                                "Define the minimum reproductibility of the "
                                "peptide to be conserved. default:1"),
    QCoreApplication::translate("main", "int"),
    QString("1"));


  QCommandLineOption countOption(
    QStringList() << "c"
                  << "count",
    QCoreApplication::translate(
      "main", "no grouping, only count proteins, unique peptides and exit"),
    QString(),
    QString());
  QCommandLineOption nomassOption(
    QStringList() << "n"
                  << "nomass",
    QCoreApplication::translate(
      "main", "grouping using only peptide sequence, no mass delta"),
    QString(),
    QString());

  QCommandLineOption fastaOption(
    QStringList() << "fasta",
    QCoreApplication::translate(
      "main", "Remove contaminant proteins from this fasta file default:no"),
    QCoreApplication::translate("main", "FILE"),
    QString());
  QCommandLineOption decoyOption(
    QStringList() << "decoy-regexp",
    QCoreApplication::translate(
      "main",
      "regular expression to select decoy protein accessions and remove them "
      "before grouping. default : .*\\|reversed$"),
    QCoreApplication::translate("main", "string"),
    QString());

  parser.addOption(inputOption);
  parser.addOption(outputOption);
  parser.addOption(pepbyprotOption);
  parser.addOption(pepreproOption);
  parser.addOption(nomassOption);

  parser.addOption(fastaOption);
  parser.addOption(decoyOption);
  parser.addOption(countOption);

  qDebug() << "GpParams::GpParams() 1";

  // Process the actual command line arguments given by the user
  parser.process(*p_app);

  qDebug() << "GpParams::GpParams() 2";

  QString value = parser.value(fastaOption);
  if(value.isEmpty())
    {
      // throw GpError(QString("--fasta need argument"));
    }
  else
    {
      this->setFastaFile(value);
    }


  _only_peptide_sequence = parser.isSet(nomassOption);
  _only_count            = parser.isSet(countOption);


  value = parser.value(decoyOption);
  if(value.isEmpty())
    {
      // throw GpError(QString("--decoy-regexp need argument"));
    }
  else
    {
      this->setRegexpDecoy(value);
    }


  value = parser.value(outputOption);
  if(value.isEmpty())
    {
      // throw GpError(QString("-o|--outfile need argument"));
    }
  else
    {
      this->setOuputStream(value);
    }


  value = parser.value(inputOption);
  if(value.isEmpty())
    {
      // throw GpError(QString("-i|--infile need argument"));
    }
  else
    {
      this->setInputStream(value);
    }


  value = parser.value(pepbyprotOption);
  if(value.isEmpty())
    {
      // throw GpError(QString("--pepbyprot need argument"));
    }
  else
    {
      this->setPepByProt(value);
    }


  value = parser.value(pepreproOption);
  if(value.isEmpty())
    {
      // throw GpError(QString("--peprepro need argument"));
    }
  else
    {
      this->setPepRepro(value);
    }

  qDebug() << "GpParams::GpParams end";
}

GpParams::~GpParams()
{
  qDebug() << "Delete GpParams";
  if(_p_outfile != NULL)
    {
      _p_outfile->close();
      delete _p_outfile;
    }
  if(_p_infile != NULL)
    {
      _p_infile->close();
      delete _p_infile;
    }
  if(_p_fasta != NULL)
    {
      delete(_p_fasta);
    }
}

bool
GpParams::getOnlyPeptideSequence() const
{
  return _only_peptide_sequence;
}
bool
GpParams::getOnlyCount() const
{
  return _only_count;
}


void
GpParams::setInputStream(QString value)
{
  _p_infile = new QFile(value);
  if(!_p_infile->exists() ||
     !_p_infile->open(QIODevice::ReadOnly | QIODevice::Text))
    throw GpError(QString("-i|--infile need valid file argument"));
  delete(_p_in);
  _p_in = new QTextStream(_p_infile);
  cerr << "New input : " << value.toStdString() << endl;
}

void
GpParams::setOuputStream(QString value)
{
  QFile *p_outfile = new QFile(value);
  if(!p_outfile->open(QIODevice::WriteOnly | QIODevice::Text))
    throw GpError(QString("-o|--outfile need valid file argument"));
  _p_outfile->close();
  delete(_p_outfile);
  _p_outfile = p_outfile;
  cerr << "New output : " << value.toStdString() << endl;
}

void
GpParams::setFastaFile(QString value)
{
  QFile *p_fasta = new QFile(value);
  if(!p_fasta->exists())
    {
      throw GpError(QString("--fasta file %1 doesn't exist")
                      .arg(QFileInfo(value).absoluteFilePath()));
    }
  else if(!p_fasta->open(QIODevice::ReadOnly | QIODevice::Text))
    {
      throw GpError(QString("--fasta file %1 is not readable")
                      .arg(QFileInfo(value).absoluteFilePath()));
    }
  if(_p_fasta != NULL)
    {
      _p_fasta->close();
      delete(_p_fasta);
    }
  _p_fasta = p_fasta;
  cerr << "Set Fasta file filtering : " << value.toStdString() << endl;
}

void
GpParams::setPepByProt(QString value)
{
  bool test  = true;
  _pepByProt = value.toInt(&test);
  if(!test)
    throw GpError(QString("--pepbyprot need int argument"));
  cerr << "New pepbyprot : " << _pepByProt << endl;
}

void
GpParams::setPepRepro(QString value)
{
  bool test = true;
  _pepRepro = value.toInt(&test);
  if(!test)
    throw GpError(QString("--peprepro need int argument"));
  cerr << "New peprepro : " << _pepRepro << endl;
}

void
GpParams::setRegexpDecoy(QString decoy_pattern)
{
  _decoy_regexp.setPattern(decoy_pattern);
}
QRegExp
GpParams::getRegexpDecoy() const
{
  return _decoy_regexp;
}
