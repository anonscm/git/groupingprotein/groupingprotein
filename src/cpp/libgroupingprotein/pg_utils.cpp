/*
 *
 * Protein Grouper
 * Copyright (C) 2014 Olivier Langella, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "pg_utils.h"
#include <cmath>

const QString
PgUtils::getLexicalOrderedString(unsigned int num)
{
  int size = log10(num);
  size += 97;
  QString base(size);
  base.append(QString().setNum(num));
  return (base);
}


void
PgUtils::writeLexicalOrderedString(QTextStream *p_out, unsigned int num)
{
  *p_out << (char)(log10(num) + 97) << num;
}
