/*
 *
 * Protein Grouper
 * Copyright (C) 2014 Olivier Langella, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *

 * peptide_set.cpp
 *
 *  Created on: 8 mars 2013 by Author: valot
 *
 */

#include "peptide_set.h"
#include <QDebug>
#include "../gp_error.h"

PeptideSet::PeptideSet()
{
}

PeptideSet::~PeptideSet()
{
  _peptides.clear();
}
const QString
PeptideSet::getFirstAlphabeticalPeptide() const
{
  if(_peptides.empty())
    {
      throw new GpError("something is wrong : this PeptideSet has no Peptide");
    }
  QString access, temp;
  for(std::set<Peptide *>::iterator it = _peptides.begin();
      it != _peptides.end();
      ++it)
    {
      temp = (*it)->getPeptideName();
      if(access.isEmpty())
        access = temp;
      else if(temp < access)
        access = temp;
    }
  return access;
}

bool
PeptideSet::containsAll(const PeptideSet &peptideSet) const
{
  for(std::set<Peptide *>::iterator incoming = peptideSet.getList().begin();
      incoming != peptideSet.getList().end();
      ++incoming)
    {
      if(find(_peptides.begin(), _peptides.end(), *incoming) == _peptides.end())
        {
          return (false);
        }
    }
  return (true);
}

bool
PeptideSet::containsOnePeptideIn(const PeptideSet &peptideSet) const
{
  for(std::set<Peptide *>::iterator incoming = peptideSet.getList().begin();
      incoming != peptideSet.getList().end();
      ++incoming)
    {
      if(find(_peptides.begin(), _peptides.end(), *incoming) != _peptides.end())
        {
          return (true);
        }
    }
  return (false);
}

void
PeptideSet::removeAll(const std::set<Peptide *> &peptidesToRemove)
{
  std::set<Peptide *>::const_iterator incoming, it;
  for(incoming = peptidesToRemove.begin(); incoming != peptidesToRemove.end();
      ++incoming)
    {
      it = find(_peptides.begin(), _peptides.end(), *incoming);
      if(it != _peptides.end())
        {
          _peptides.erase(it);
        }
    }
}

unsigned int
PeptideSet::getTotalSpectra() const
{
  unsigned int total = 0;
  for(std::set<Peptide *>::const_iterator it = _peptides.begin();
      it != _peptides.end();
      it++)
    {
      total += (*it)->getTotalSpectra();
    }
  return total;
}
