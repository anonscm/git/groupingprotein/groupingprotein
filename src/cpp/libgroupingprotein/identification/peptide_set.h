/*
 *
 * Protein Grouper
 * Copyright (C) 2014 Olivier Langella, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *

 * peptide_set.h
 *
 *  Created on: 8 mars 2013 by Author: valot
 *
 */

#include <set>
#include "peptide.h"

#ifndef PEPTIDE_SET_H_
#define PEPTIDE_SET_H_

// struct pepcomp {
//	bool operator()(const Peptide* lhs, const Peptide* rhs) const {
//    	if(lhs->getMh() != rhs->getMh())
//    		return ( lhs->getMh() < rhs->getMh() );
//    	else
//    		return(lhs->getSequenceLi() < rhs->getSequenceLi());
//	}
//};

class PeptideSet
{
  public:
  PeptideSet();
  virtual ~PeptideSet();

  inline void
  insert(Peptide *pep)
  {
    this->_peptides.insert(pep);
  }

  bool containsAll(const PeptideSet &peptideSet) const;

  inline bool
  contains(Peptide *pep) const
  {
    return (find(_peptides.begin(), _peptides.end(), pep) != _peptides.end());
  }

  void removeAll(const std::set<Peptide *> &peptidesToRemove);

  inline void
  addAll(const PeptideSet &peptideSet)
  {
    _peptides.insert(peptideSet.getList().begin(), peptideSet.getList().end());
  }

  bool containsOnePeptideIn(const PeptideSet &peptideSet) const;

  inline unsigned int
  size() const
  {
    return _peptides.size();
  }

  inline const std::set<Peptide *> &
  getList() const
  {
    return _peptides;
  }

  void
  clear()
  {
    return _peptides.clear();
  }

  unsigned int getTotalSpectra() const;

  const QString getFirstAlphabeticalPeptide() const;

  private:
  std::set<Peptide *> _peptides;
};

#endif /* PEPTIDE_SET_H_ */
