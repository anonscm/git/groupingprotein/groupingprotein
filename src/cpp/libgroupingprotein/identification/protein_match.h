/*
 * protein_match.h
 *
 *  Created on: 8 mars 2013
 *      Author: valot
 */

#include "peptide_set.h"
#include <QTextStream>
#include <pappsomspp/grouping/grpexperiment.h>

#ifndef PROTEIN_MATCH_H_
#define PROTEIN_MATCH_H_

class ProteinMatch
{
  public:
  ProteinMatch(QString access, QString desc);
  virtual ~ProteinMatch();

  void addPeptides(Peptide *pep);

  const PeptideSet &
  getPeptideSet() const
  {
    return _peptideSet;
  }

  QString
  getAccession() const
  {
    return _accession;
  }

  QString
  getDescription() const
  {
    return _description;
  }

  void removePeptide(std::set<Peptide *> &peptidesToRemove);

  unsigned int getUniquePeptide() const;

  void writeFastaPeptides(QTextStream *p_out) const;

  bool hasCommonPeptideWith(const ProteinMatch &protMatch) const;
  void setGroupingExperiment(pappso::GrpExperiment *_p_grp_experiment);

  const pappso::GrpProteinSp &getGrpProteinSp() const;

  private:
  const QString _accession;
  const QString _description;
  PeptideSet _peptideSet;

  pappso::GrpProteinSp _sp_grp_protein;
};

#endif /* PROTEIN_MATCH_H_ */
