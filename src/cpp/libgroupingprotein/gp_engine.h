/*
 * gp_engine.h
 *
 *  Created on: 7 mars 2013
 *      Author: valot
 */

#include "gp_params.h"
#include "gp_types.h"
#include "identification/peptide_set.h"
#include "identification/protein_match.h"
#include "identification/peptide.h"
#include <pappsomspp/grouping/grpexperiment.h>

#pragma once

class GpEngine
{
  public:
  GpEngine(GpParams *params);
  virtual ~GpEngine();
  void performedTraitment();

  Peptide *getPeptideInstance(QString seq, gp_double mh);

  ProteinMatch *getProteinMatchInstance(QString access, QString desc);

  Sample *getSampleInstance(QString name);

  // Iterator
  std::map<QString, Sample *>::const_iterator
  getSamplesBegin() const
  {
    return (_sampleList.begin());
  }
  std::map<QString, Sample *>::const_iterator
  getSamplesEnd() const
  {
    return (_sampleList.end());
  }

  pappso::GrpExperiment *
  getGrpExperiment()
  {
    return _p_grp_experiment;
  }

  std::map<QString, ProteinMatch *>
  getProteinMatchList() const
  {
    return _proteinMatchList;
  }

  std::vector<const Peptide *>
  getPeptideListInGroup(unsigned int group_number) const;

  private:
  void readInputStream();
  void filterResult();
  void performedGrouping();
  void writeXmlOutputResult();

  protected:
  GpParams *_p_params;


  private:
  pappso::GrpGroupingMonitorInterface *_p_monitor = nullptr;
  pappso::GrpExperiment *_p_grp_experiment;


  std::map<QString, Peptide *> _peptideList;
  /* Unique peptide to all dataset*/
  std::map<QString, ProteinMatch *> _proteinMatchList;
  /* accession are used as key to retreived proteinMatch ref*/
  std::map<QString, Sample *> _sampleList;
  /* name are used as key to retrieve sample ref */
};
