/*
 * fasta_filter.cpp
 *
 *  Created on: 27 mars 2013
 *      Author: valot
 */

#include "fasta_filter.h"
#include <QDebug>

FastaFilter::FastaFilter(GpParams *params)
{
  _p_params = params;
}

FastaFilter::~FastaFilter()
{
  // TODO Auto-generated destructor stub
}

void
FastaFilter::filterResult(GpEngine *p_engine)
{
  if(!_p_params->isFastaFile())
    return;
  QFile *fasta = _p_params->getFastaFile();

  std::set<QString> accessions;
  // Search accession conta
  QTextStream in(fasta);
  while(!in.atEnd())
    {
      QString line = in.readLine();
      if(line.startsWith(">"))
        {
          QStringList elements = line.split(" ");
          if(elements.size() > 0)
            {
              QString access = elements.at(0);
              accessions.insert(access.remove(0, 1));
            }
        }
    }
  fasta->close();

  // Remove contaminants
  ProteinMatch *p_protein;
  for(std::pair<QString, ProteinMatch *> protein_match_list :
      p_engine->getProteinMatchList())
    {
      p_protein = protein_match_list.second;
      if(accessions.count(p_protein->getAccession()) == 1)
        {
          qDebug() << "Remove Contaminant : " << p_protein->getAccession();
          p_engine->getGrpExperiment()->addPostGroupingGrpProteinSpRemoval(
            p_protein->getGrpProteinSp());
        }
    }
  std::cerr << "Remove protein contaminants from fasta file : "
            << accessions.size() << std::endl;
}
