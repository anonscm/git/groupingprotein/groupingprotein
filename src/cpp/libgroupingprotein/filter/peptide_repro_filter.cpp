/*
 * peptide_repro_filter.cpp
 *
 *  Created on: 20 mars 2013
 *      Author: valot
 */

#include "peptide_repro_filter.h"
#include <QDebug>

PeptideReproFilter::PeptideReproFilter(GpParams *params) : DataFilter(params)
{
}

PeptideReproFilter::~PeptideReproFilter()
{
}

void
PeptideReproFilter::filterResult(
  std::map<QString, Peptide *> &peptideList,
  std::map<QString, ProteinMatch *> &proteinMatchList)
{
  // remove peptide not reproductibly identified in at least X samples
  std::cerr << "Begin Peptide Reproductibility filter : " << peptideList.size()
            << std::endl;
  std::map<QString, Peptide *>::iterator peptideIt, peptideItRemove;
  std::set<Peptide *> peptideToRemove;
  Peptide *p_peptide;
  unsigned int pepRepro = _p_params->getPepRepro();
  for(peptideIt = peptideList.begin(); peptideIt != peptideList.end();)
    {
      p_peptide = peptideIt->second;
      if(!p_peptide->isReproductible(pepRepro))
        {
          qDebug() << "Not reproductible peptide : "
                   << p_peptide->getSequenceLi() << p_peptide->getMh();
          peptideToRemove.insert(p_peptide);
          peptideItRemove = peptideIt;
          peptideIt++;
          peptideList.erase(peptideItRemove);
        }
      else
        {
          qDebug() << "Reproductible peptide : " << p_peptide->getSequenceLi()
                   << p_peptide->getMh();
          peptideIt++;
        }
    }

  // Remove instance from proteinList
  std::map<QString, ProteinMatch *>::iterator proteinIt;
  ProteinMatch *p_protein;
  for(proteinIt = proteinMatchList.begin(); proteinIt != proteinMatchList.end();
      proteinIt++)
    {
      p_protein = proteinIt->second;
      p_protein->removePeptide(peptideToRemove);
    }

  // delete peptide
  std::set<Peptide *>::iterator it;
  for(it = peptideToRemove.begin(); it != peptideToRemove.end(); it++)
    {
      p_peptide = *it;
      // qDebug()<<"Delete peptide : "<<
      // p_peptide->getSequenceLi()<<p_peptide->getMh();
      delete(p_peptide);
    }
  peptideToRemove.clear();
  std::cerr << "End Peptide Reproductibility filter : " << peptideList.size()
            << std::endl;
}
