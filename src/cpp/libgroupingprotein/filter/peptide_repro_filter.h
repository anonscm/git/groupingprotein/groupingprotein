/*
 * peptide_repro_filter.h
 *
 *  Created on: 20 mars 2013
 *      Author: valot
 */

#ifndef PEPTIDE_REPRO_FILTER_H_
#define PEPTIDE_REPRO_FILTER_H_

#include "data_filter.h"

class PeptideReproFilter : public DataFilter
{
  public:
  PeptideReproFilter(GpParams *params);
  virtual ~PeptideReproFilter();
  void filterResult(std::map<QString, Peptide *> &peptideList,
                    std::map<QString, ProteinMatch *> &proteinMatchList);
};

#endif /* PEPTIDE_REPRO_FILTER_H_ */
