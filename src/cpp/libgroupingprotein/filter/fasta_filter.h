/*
 * fasta_filter.h
 *
 *  Created on: 27 mars 2013
 *      Author: valot
 */

#ifndef FASTA_FILTER_H_
#define FASTA_FILTER_H_

#include "data_filter.h"

class FastaFilter
{
  public:
  FastaFilter(GpParams *params);
  virtual ~FastaFilter();
  void filterResult(GpEngine *p_engine);

  private:
  GpParams *_p_params;
};

#endif /* FASTA_FILTER_H_ */
