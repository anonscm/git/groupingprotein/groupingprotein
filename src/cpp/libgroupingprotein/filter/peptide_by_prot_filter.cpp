/*
 * peptide_by_prot.cpp
 *
 *  Created on: 21 mars 2013
 *      Author: valot
 */

#include "peptide_by_prot_filter.h"
#include <QDebug>

PeptideByProtFilter::PeptideByProtFilter(GpParams *params) : DataFilter(params)
{
}

PeptideByProtFilter::~PeptideByProtFilter()
{
}

void
PeptideByProtFilter::filterResult(
  std::map<QString, Peptide *> &peptideList,
  std::map<QString, ProteinMatch *> &proteinMatchList)
{
  // remove protein not identified with at least n uniques peptides
  unsigned int pepByProt = _p_params->getPepByProt();
  std::cerr << "Begin Minimun Peptide By Prot filter : "
            << proteinMatchList.size() << std::endl;

  std::map<QString, ProteinMatch *>::iterator proteinIt, proteinToRemove;
  ProteinMatch *p_protein;
  for(proteinIt = proteinMatchList.begin();
      proteinIt != proteinMatchList.end();)
    {
      p_protein = proteinIt->second;
      if(p_protein->getUniquePeptide() < pepByProt)
        {
          qDebug() << "Bad protein " << p_protein->getAccession();
          proteinToRemove = proteinIt;
          proteinIt++;
          proteinMatchList.erase(proteinToRemove);
          delete(p_protein);
        }
      else
        {
          proteinIt++;
          qDebug() << "Good protein " << p_protein->getAccession();
        }
    }
  std::cerr << "End Minimun Peptide By Prot filter : "
            << proteinMatchList.size() << std::endl;
}
