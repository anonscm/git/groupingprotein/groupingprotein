/*
 * data_filter.h
 *
 *  Created on: 20 mars 2013
 *      Author: valot
 */

#ifndef DATA_FILTER_H_
#define DATA_FILTER_H_

#include "../gp_engine.h"

class DataFilter
{
  public:
  DataFilter(GpParams *params);
  virtual ~DataFilter();

  virtual void
  filterResult(std::map<QString, Peptide *> &peptideList,
               std::map<QString, ProteinMatch *> &proteinMatchList) = 0;

  protected:
  GpParams *_p_params;
};

#endif /* DATA_FILTER_H_ */
