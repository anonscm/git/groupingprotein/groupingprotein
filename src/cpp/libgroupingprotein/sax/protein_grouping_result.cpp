/*
 * protein_grouping_result.cpp
 *
 *  Created on: 25 mars 2013
 *      Author: valot
 */

#include "protein_grouping_result.h"
#include <pappsomspp/grouping/grpgroup.h>
#include <pappsomspp/grouping/grpexperiment.h>
#include <QDebug>
#include <cmath>
#include <tuple>
#include <pappsomspp/pappsoexception.h>

ProteinGroupingResult::ProteinGroupingResult(GpParams *params) : peptideId(0)
{
  _writer = new QXmlStreamWriter(params->getOutputFile());
  _writer->setAutoFormatting(true);
  this->printHeader();
  this->printParams(params);
}

ProteinGroupingResult::~ProteinGroupingResult()
{
  qDebug() << "Delete writer";
  this->printFooter();
  delete(_writer);
  _sampleId.clear();
  _proteinId.clear();
}

void
ProteinGroupingResult::printHeader()
{
  qDebug() << "Print Header";
  _writer->writeStartDocument();
  _writer->writeStartElement("grouping_protein");
}

void
ProteinGroupingResult::printParams(GpParams *params)
{
  qDebug() << "Print params";
  _writer->writeStartElement("params");
  _writer->writeEmptyElement("peptide_reproductibility");
  _writer->writeAttribute("value", QString().setNum(params->getPepRepro()));
  _writer->writeEmptyElement("peptide_by_prot");
  _writer->writeAttribute("value", QString().setNum(params->getPepByProt()));
  _writer->writeEmptyElement("remove_database");
  if(params->isFastaFile())
    _writer->writeAttribute("value", params->getFastaFile()->fileName());
  // End params element
  _writer->writeEndElement();
}

void
ProteinGroupingResult::printGroupingResult(GpEngine *engine)
{
  qDebug() << "Begin print GpEngine result";
  _p_engine = engine;
  this->printSamples(engine);
  this->printProteins(engine);
  this->printGroups(engine);
}

void
ProteinGroupingResult::printSamples(GpEngine *engine)
{
  qDebug() << "Print Samples";
  _writer->writeStartElement("sample_list");
  std::map<QString, Sample *>::const_iterator samples;
  std::vector<QString>::const_iterator files;
  Sample *samp;
  QString id;
  int sampleCount = 1;
  for(samples = engine->getSamplesBegin(); samples != engine->getSamplesEnd();
      samples++)
    {
      samp = samples->second;
      id.clear();
      id.append("samp").append(QString().setNum(sampleCount));
      _writer->writeStartElement("sample");
      _writer->writeAttribute("id", id);
      _writer->writeAttribute("name", samp->getName());
      for(files = samp->getFiles().begin(); files != samp->getFiles().end();
          files++)
        {
          _writer->writeEmptyElement("xtandem");
          _writer->writeAttribute("file", (*files));
        }
      // end Sample
      _writer->writeEndElement();
      // Store id
      _sampleId.insert(std::pair<Sample *, QString>(samp, id));
      sampleCount++;
    }
  // end Sample_list
  _writer->writeEndElement();
}

void
ProteinGroupingResult::printProteins(GpEngine *engine)
{
  qDebug() << "ProteinGroupingResult::printProteins begin";
  _writer->writeStartElement("protein_list");

  std::vector<ProteinMatch *> protein_match_list;
  for(auto &pair_protein : engine->getProteinMatchList())
    {
      if((pair_protein.second->getGrpProteinSp() != nullptr) &&
         (pair_protein.second->getGrpProteinSp().get()->getGroupNumber() > 0))
        {
          protein_match_list.push_back(pair_protein.second);
        }
    }
  std::sort(protein_match_list.begin(),
            protein_match_list.end(),
            [](const ProteinMatch *a, const ProteinMatch *b) {
              unsigned int ag = a->getGrpProteinSp().get()->getGroupNumber();
              unsigned int bg = b->getGrpProteinSp().get()->getGroupNumber();

              unsigned int asg =
                a->getGrpProteinSp().get()->getSubGroupNumber();
              unsigned int bsg =
                b->getGrpProteinSp().get()->getSubGroupNumber();
              unsigned int arank = a->getGrpProteinSp().get()->getRank();
              unsigned int brank = b->getGrpProteinSp().get()->getRank();
              return std::tie(ag, asg, arank) < std::tie(bg, bsg, brank);
            });


  for(ProteinMatch *p_protein : protein_match_list)
    {
      try
        {
          _writer->writeEmptyElement("protein");
          _writer->writeAttribute(
            "id", p_protein->getGrpProteinSp().get()->getGroupingId());
          _writer->writeAttribute("access", p_protein->getAccession());
          _writer->writeAttribute("desc", p_protein->getDescription());
        }
      catch(pappso::PappsoException & error)
        {
          throw pappso::PappsoException(
            QObject::tr("Error writing protein %1 :\n%2")
              .arg(p_protein->getAccession())
              .arg(error.qwhat()));
        }
      catch(std::exception & error)
        {
          throw pappso::PappsoException(
            QObject::tr("std Error writing protein %1 :\n%2")
              .arg(p_protein->getAccession())
              .arg(error.what()));
        }
    }
  // end of prot_list
  _writer->writeEndElement();
  qDebug() << "ProteinGroupingResult::printProteins end";
}

void
ProteinGroupingResult::printGroups(GpEngine *engine)
{
  qDebug() << "ProteinGroupingResult::printGroups begin";
  _writer->writeStartElement("group_list");

  for(pappso::GrpGroupSpConst group :
      engine->getGrpExperiment()->getGrpGroupSpList())
    {
      this->printGroup(group);
    }

  // end of group_list
  _writer->writeEndElement();
}

void
ProteinGroupingResult::printGroup(pappso::GrpGroupSpConst group)
{
  qDebug() << "ProteinGroupingResult::printGroup begin";
  _writer->writeStartElement("group");
  _writer->writeAttribute("id", group.get()->getGroupingId());
  _writer->writeAttribute("number",
                          QString().setNum(group.get()->getGroupNumber()));
  //_writer->writeAttribute("count",
  //                        QString().setNum(group->getPeptideSet().getTotalSpectra()));
  _writer->writeStartElement("subgroup_list");


  for(pappso::GrpSubGroupSpConst &subgroup_sp :
      group.get()->getGrpSubGroupSpList())
    {

      _writer->writeEmptyElement("subgroup");
      _writer->writeAttribute("id", subgroup_sp.get()->getGroupingId());
      //_writer->writeAttribute("count",
      //                       QString().setNum(subgroup->getPeptideSet().getTotalSpectra()));
      _writer->writeAttribute(
        "number", QString().setNum(subgroup_sp.get()->getSubGroupNumber()));

      QStringList prot_ids;
      for(const pappso::GrpProtein *protein :
          subgroup_sp.get()->getGrpProteinList())
        {
          prot_ids << protein->getGroupingId();
        }
      _writer->writeAttribute("protIds", prot_ids.join(" "));
    }
  // end of subgroupList
  _writer->writeEndElement();
  // print PeptideList of this group
  this->printPeptideList(group);
  // end of group
  _writer->writeEndElement();
  qDebug() << "ProteinGroupingResult::printGroup end";
}

void
ProteinGroupingResult::printPeptideList(pappso::GrpGroupSpConst group)
{
  qDebug() << "ProteinGroupingResult::printPeptideList begin";
  _writer->writeStartElement("peptide_list");

  std::vector<const Peptide *> peptide_list =
    _p_engine->getPeptideListInGroup(group.get()->getGroupNumber());

  qDebug() << "ProteinGroupingResult::printPeptideList peptide_list.size="
           << peptide_list.size();

  std::set<Peptide *>::const_iterator pepIt;
  // std::vector<SubGroup *>::const_iterator subIt;
  std::map<QString, int>::const_iterator seqIt;
  std::map<Sample *, std::map<int, int>>::const_iterator sampleIt;
  std::map<int, int>::const_iterator scansIt;
  // Peptide * pep;
  for(const Peptide *p_peptide : peptide_list)
    {
      // for (pepIt = group->getPeptideSet().getList().begin();
      //       pepIt != group->getPeptideSet().getList().end(); pepIt++) {
      //  peptideId++;
      //  pep = *pepIt;
      _writer->writeStartElement("peptide");
      qDebug() << "ProteinGroupingResult::printPeptideList a";
      _writer->writeAttribute(
        "id", p_peptide->getGrpPeptideSp().get()->getGroupingId());
      qDebug() << "ProteinGroupingResult::printPeptideList b";
      _writer->writeAttribute("seqLI", p_peptide->getSequenceLi());
      _writer->writeAttribute("mhTheo",
                              QString().setNum(p_peptide->getMh(), 'g', 8));
      // SubGroupIDs
      QStringList sg_ids;
      for(pappso::GrpSubGroupSpConst subgroup_sp :
          group.get()->getGrpSubGroupSpList())
        {
          if(subgroup_sp.get()->getPeptideSet().contains(
               p_peptide->getGrpPeptideSp().get()))
            {
              sg_ids << subgroup_sp.get()->getGroupingId();
            }
        }
      qDebug() << "ProteinGroupingResult::printPeptideList c";
      _writer->writeAttribute("subgroupIds", sg_ids.join(" "));

      // Sequence Modifs
      _writer->writeStartElement("sequence_modifs_list");
      for(seqIt = p_peptide->getSequenceModifsBegin();
          seqIt != p_peptide->getSequenceModifsEnd();
          seqIt++)
        {
          _writer->writeEmptyElement("sequence_modifs");
          QStringList list = seqIt->first.split("|");
          _writer->writeAttribute("seq", list.value(0));
          _writer->writeAttribute("mods", list.value(1));
          _writer->writeAttribute("count", QString().setNum(seqIt->second));
        }
      qDebug() << "ProteinGroupingResult::printPeptideList d";
      // end sequence modif list$
      _writer->writeEndElement();
      // spectra
      _writer->writeStartElement("spectra");
      for(sampleIt = p_peptide->getHashSampleScanBegin();
          sampleIt != p_peptide->getHashSampleScanEnd();
          sampleIt++)
        {
          QString sampId = _sampleId.find(sampleIt->first)->second;
          for(scansIt = sampleIt->second.begin();
              scansIt != sampleIt->second.end();
              scansIt++)
            {
              _writer->writeEmptyElement("spectrum");
              _writer->writeAttribute("sample", sampId);
              _writer->writeAttribute("scan", QString().setNum(scansIt->first));
              _writer->writeAttribute("charge",
                                      QString().setNum(scansIt->second));
            }
        }
      qDebug() << "ProteinGroupingResult::printPeptideList e";
      // end spectra
      _writer->writeEndElement();
      // end of Peptide
      _writer->writeEndElement();
    }
  // end of peptideList
  _writer->writeEndElement();
  qDebug() << "ProteinGroupingResult::printPeptideList end";
}

void
ProteinGroupingResult::printFooter()
{
  // End of protein_grouping element
  _writer->writeEndElement();
  _writer->writeEndDocument();
}

QString
ProteinGroupingResult::getId(int num)
{
  int size = log10(num);
  size += 97;
  QString base(size);
  base.append(QString().setNum(num));
  return (base);
}
