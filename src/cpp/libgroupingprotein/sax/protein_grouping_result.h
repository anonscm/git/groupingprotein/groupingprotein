/*
 * protein_grouping_result.h
 *
 *  Created on: 25 mars 2013
 *      Author: valot
 */

#ifndef PROTEIN_GROUPING_RESULT_H_
#define PROTEIN_GROUPING_RESULT_H_

#include <QXmlStreamWriter>
#include "../gp_engine.h"
#include "../gp_params.h"

class ProteinGroupingResult
{
  public:
  ProteinGroupingResult(GpParams *_params);
  virtual ~ProteinGroupingResult();
  void printGroupingResult(GpEngine *engine);

  private:
  void printHeader();
  void printParams(GpParams *params);
  void printSamples(GpEngine *engine);
  void printProteins(GpEngine *engine);
  void printGroups(GpEngine *engine);
  void printGroup(pappso::GrpGroupSpConst group);
  void printPeptideList(pappso::GrpGroupSpConst group);
  // void printSubgroup(SubGroup * subGroup);
  void printFooter();
  QString getId(int num);

  private:
  GpEngine *_p_engine;
  QXmlStreamWriter *_writer;
  std::map<Sample *, QString> _sampleId;
  std::map<const ProteinMatch *, QString> _proteinId;
  // std::map<SubGroup *, QString> _subgroupId;
  int peptideId;
};

#endif /* PROTEIN_GROUPING_RESULT_H_ */
