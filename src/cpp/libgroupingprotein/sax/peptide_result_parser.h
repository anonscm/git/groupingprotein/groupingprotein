/*
 * peptide_result_parser.h
 *
 *  Created on: 8 mars 2013
 *      Author: valot
 */

#ifndef PEPTIDE_RESULT_PARSER_H_
#define PEPTIDE_RESULT_PARSER_H_

#include <QXmlDefaultHandler>
#include <pappsomspp/peptide/peptide.h>
#include "../gp_engine.h"

class PeptideResultParser : public QXmlDefaultHandler
{
  public:
  PeptideResultParser(GpEngine *engine);
  virtual ~PeptideResultParser();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);

  QString errorString() const;

  private:
  QString getAccessionTodesc(QString &);

  /// current parsed text
  QString _currentText;

  /// error message during parsing
  QString _errorStr;

  /// GpEngine
  GpEngine *_engine;

  Sample *_p_currentSample;

  QString _currentSequence;

  QString _currentModifs;

  ProteinMatch *_p_currentProtein;

  int _currentScan;

  int _currentCharge;

  gp_double _currentMH;

  pappso::NoConstPeptideSp _sp_current_peptide;

  bool _firstElement;
};

#endif /* PEPTIDE_RESULT_PARSER_H_ */
