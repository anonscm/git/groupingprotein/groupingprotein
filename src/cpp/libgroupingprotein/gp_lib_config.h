#ifndef _CONFIG_H
#define _CONFIG_H

#define GP_VERSION "0.5.2"
#define QT_V_4_5 0x040500
#define QT_V_4_6 0x040600

#include <QDebug>

#include "gp_types.h"


#endif /* _CONFIG_H */
