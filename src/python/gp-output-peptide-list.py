#!/usr/bin/python

import xml.sax


#Class Defined Sax Handler
class OutputPeptideListHandler(xml.sax.ContentHandler):
    def __init__(self, outfile):
        xml.sax.ContentHandler.__init__(self)
        self.outfile = outfile
        self.groupId = ""
        self.pepId = ""
        self.subs = ""
        self.MhTheo = ""
        self.seqMods = []
        self.count = 0
        self.z = set()
        self.firstElement = True

    def startElement(self, name, attrs):
        if self.firstElement:
            self.firstElement = False
            if name != "grouping_protein":
                raise Exception("The input is not a gp result file")
        elif name == "group_list":
            self.outfile.write("Group\tPeptide\tSequence\tModifs\t")
            self.outfile.write("MhTheo\tCharges\tSubgroups\tSpectra\n")
        elif name == "group":
            self.groupId = attrs.get('id')
        elif name == "subgroup":
            sub = attrs.get('id')
        elif name == "peptide":
            self.pepId = attrs.get('id')
            self.MhTheo = attrs.get('mhTheo')
            self.subs = attrs.get('subgroupIds')
            self.seqMods = []
            self.count = 0
            self.z.clear()
        elif name == "sequence_modifs":
            seqMod = attrs.get('seq') + "\t" + attrs.get('mods')
            self.seqMods.append(seqMod)
        elif name == "spectrum":
            self.count += 1
            self.z.add(attrs.get('charge'))

    def endElement(self, name):
        if name == "peptide":
            for seqMod in self.seqMods:
                self.outfile.write(self.groupId + "\t" + self.pepId \
                    + "\t" + seqMod + "\t" + self.MhTheo + "\t" \
                    + ('|').join(sorted(list(self.z))) + "\t" \
                    + self.subs + "\t" + str(self.count) + "\n")

import argparse
import sys

#Defined command line
command = argparse.ArgumentParser(prog='gp-output-peptide-list', \
    description='Export peptide list on gp-grouping result.', \
    usage='%(prog)s [options]')
command.add_argument('-i', '--infile', nargs="?", \
    type=argparse.FileType("r"), default=sys.stdin, \
    help='Open gp-grouping result default:STDIN')
command.add_argument('-o', '--outfile', nargs="?", \
    type=argparse.FileType("w"), default=sys.stdout, \
    help='Save peptide list on default:STDOUT')
command.add_argument('-v', '--version', action='version', \
    version='%(prog)s ${GP_VERSION}')

#Read arguments of command line
args = command.parse_args()

#Parse the input with xml sax reader
parser = xml.sax.make_parser()
parser.setContentHandler(OutputPeptideListHandler(args.outfile))
parser.parse(args.infile)
