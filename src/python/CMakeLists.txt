#configuring python path on script using xmlstreamwriter library
configure_file (${CMAKE_CURRENT_SOURCE_DIR}/gp-read-xtandem.py ${CMAKE_CURRENT_BINARY_DIR}/gp-read-xtandem.py @ONLY)
configure_file (${CMAKE_CURRENT_SOURCE_DIR}/gp-output-masschroqml.py ${CMAKE_CURRENT_BINARY_DIR}/gp-output-masschroqml.py @ONLY)

#copie script without xmlstreamwriter library
configure_file (${CMAKE_CURRENT_SOURCE_DIR}/gp-fdr-peptide.py ${CMAKE_CURRENT_BINARY_DIR}/gp-fdr-peptide.py COPYONLY)
configure_file (${CMAKE_CURRENT_SOURCE_DIR}/gp-output-peptide-counting.py ${CMAKE_CURRENT_BINARY_DIR}/gp-output-peptide-counting.py COPYONLY)
configure_file (${CMAKE_CURRENT_SOURCE_DIR}/gp-output-peptide-list.py ${CMAKE_CURRENT_BINARY_DIR}/gp-output-peptide-list.py COPYONLY)
configure_file (${CMAKE_CURRENT_SOURCE_DIR}/gp-output-protein-counting.py ${CMAKE_CURRENT_BINARY_DIR}/gp-output-protein-counting.py COPYONLY)
configure_file (${CMAKE_CURRENT_SOURCE_DIR}/gp-output-protein-list.py ${CMAKE_CURRENT_BINARY_DIR}/gp-output-protein-list.py COPYONLY)
