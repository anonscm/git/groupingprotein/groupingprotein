#!/usr/bin/python

import xml.sax


#Class Defined Sax Handler
class OutputPeptideCountingHandler(xml.sax.ContentHandler):
    def __init__(self, outfile):
        xml.sax.ContentHandler.__init__(self)
        self.outfile = outfile
        self.sampleIds = {}
        self.sampleOrder = []
        self.bestSeqMods = []
        self.count = {}
        self.peptId = ""
        self.subs = ""
        self.mhTheo = ""
        self.z = set()
        self.firstElement = True

    def startElement(self, name, attrs):
        if self.firstElement:
            self.firstElement = False
            if name != "grouping_protein":
                raise Exception("The input is not a gp result file")
        elif name == "sample":
            self.sampleIds[attrs.get('id')] = attrs.get('name')
            self.sampleOrder.append(attrs.get('id'))
        elif name == "group":
            self.groupId = attrs.get('id')
        elif name == "peptide":
            self.count.clear()
            for samp in self.sampleOrder:
                self.count[samp] = 0
            self.pepId = attrs.get('id')
            self.subs = attrs.get('subgroupIds')
            self.mhTheo = attrs.get('mhTheo')
            self.z.clear()
            self.bestSeqMods = []
        elif name == "sequence_modifs":
            count = int(attrs.get('count'))
            if len(self.bestSeqMods) == 0:
                self.bestSeqMods.append(count)
                self.bestSeqMods.append(attrs.get('seq'))
                self.bestSeqMods.append(attrs.get('mods'))
            elif self.bestSeqMods[0] < count:
                self.bestSeqMods[0] = count
                self.bestSeqMods[1] = attrs.get('seq')
                self.bestSeqMods[2] = attrs.get('mods')
        elif name == "spectrum":
            self.count[attrs.get('sample')] += 1
            self.z.add(attrs.get('charge'))

    def endElement(self, name):
        if name == "sample_list":
            self.outfile.write("Group\tPeptide\tSequence\t" \
                + "Modifs\tMhTheo \tCharge\tSubgroups")
            for samp in self.sampleOrder:
                self.outfile.write("\t" + self.sampleIds[samp])
            del(self.sampleIds)
            self.outfile.write("\n")
        elif name == "peptide":
            self.__print_peptide()

    def __print_peptide(self):
        self.outfile.write(self.groupId + "\t" + self.pepId + "\t" \
            + self.bestSeqMods[1] + "\t" + self.bestSeqMods[2] + "\t" \
            + self.mhTheo + "\t" + ('|').join(sorted(list(self.z))) \
            + "\t" + self.subs)
        for samp in self.sampleOrder:
            self.outfile.write("\t" + str(self.count[samp]))
        self.outfile.write("\n")


import argparse
import sys

#Defined command line
command = argparse.ArgumentParser(prog='gp-output-peptide-counting', \
    description='Export peptide counting on gp-grouping result.', \
    usage='%(prog)s [options]')
command.add_argument('-i', '--infile', nargs="?", \
    type=argparse.FileType("r"), default=sys.stdin, \
    help='Open gp-grouping result default:STDIN')
command.add_argument('-o', '--outfile', nargs="?", \
    type=argparse.FileType("w"), default=sys.stdout, \
    help='Save peptide counting on default:STDOUT')
command.add_argument('-v', '--version', action='version', \
    version='%(prog)s ${GP_VERSION}')

#Read arguments of command line
args = command.parse_args()

#Parse the input with xml sax reader
parser = xml.sax.make_parser()
parser.setContentHandler(OutputPeptideCountingHandler(args.outfile))
parser.parse(args.infile)
