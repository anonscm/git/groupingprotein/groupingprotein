#!/usr/bin/python

import xml.sax


#Class Defined Sax Handler
class OutputProteinListHandler(xml.sax.ContentHandler):
    def __init__(self, outfile):
        xml.sax.ContentHandler.__init__(self)
        self.outfile = outfile
        self.prots = {}
        self.protIds = {}
        self.subgroupTotal = {}
        self.subgroupUnique = {}
        self.subgroupSpecific = {}
        self.seqs = {}
        self.outfile.write("Group" + "\t" + "SubGroup" + "\t")
        self.outfile.write("Protein" + "\t" + "Description" + "\t")
        self.outfile.write("Total" + "\t" + "Specific" + "\t")
        self.outfile.write("Specific Unique" + "\t" + "SubGroup count" + "\n")
        self.firstElement = True

    def startElement(self, name, attrs):
        if self.firstElement:
            self.firstElement = False
            if name != "grouping_protein":
                raise Exception("The input is not a gp result file")
        elif name == "protein":
            self.protIds[attrs.get('id')] = attrs.get('desc')
        elif name == "group":
            self.groupId = attrs.get('id')
            self.subgroup = []
            self.subgroupSpecific.clear()
            self.subgroupTotal.clear()
            self.subgroupUnique.clear()
            self.seqs.clear()
            self.prots.clear()
        elif name == "subgroup":
            sub = attrs.get('id')
            self.subgroup.append(sub)
            self.subgroupSpecific[sub] = 0
            self.subgroupTotal[sub] = 0
            self.subgroupUnique[sub] = 0
            self.prots[sub] = str(attrs.get('protIds')).split()
        elif name == "peptide":
            seq = attrs.get('seqLI')
            self.subs = attrs.get('subgroupIds').split()
            self.specific = len(self.subs) == 1
            if (seq in self.seqs.keys()) == False and self.specific:
                for sub in self.subs:
                    self.subgroupUnique[sub] += 1
            self.seqs[seq] = ""
        elif name == "spectrum":
            for sub in self.subs:
                self.subgroupTotal[sub] += 1
                if self.specific:
                    self.subgroupSpecific[sub] += 1

    def endElement(self, name):
        if name == "group":
            for sub in self.subgroup:
                self.printSubGroup(sub)
            self.outfile.write("\n")  # space betwwen group

    def printSubGroup(self, sub):
        for prots in self.prots[sub]:
            self.outfile.write(self.groupId + "\t" + sub + "\t")
            self.outfile.write(prots + "\t" + self.protIds[prots] + "\t")
            self.outfile.write(str(self.subgroupTotal[sub]) + "\t")
            self.outfile.write(str(self.subgroupSpecific[sub]) + "\t")
            self.outfile.write(str(self.subgroupUnique[sub]) + "\t")
            self.outfile.write("*" + str(len(self.prots[sub])) + "\n")

import argparse
import sys

#Defined command line
command = argparse.ArgumentParser(prog='gp-output-protein-list', \
    description='Export protein list on gp-grouping result.', \
    usage='%(prog)s [options]')
command.add_argument('-i', '--infile', nargs="?", \
    type=argparse.FileType("r"), default=sys.stdin, \
    help='Open gp-grouping result default:STDIN')
command.add_argument('-o', '--outfile', nargs="?", \
    type=argparse.FileType("w"), default=sys.stdout, \
    help='Save protein list on default:STDOUT')
command.add_argument('-v', '--version', action='version', \
    version='%(prog)s ${GP_VERSION}')

#Read arguments of command line
args = command.parse_args()

#Parse the input with xml sax reader
parser = xml.sax.make_parser()
parser.setContentHandler(OutputProteinListHandler(args.outfile))
parser.parse(args.infile)
