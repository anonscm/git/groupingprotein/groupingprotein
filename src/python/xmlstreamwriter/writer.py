import sys
from error import Error


class Writer:
    """Write xml file on stream

    Attributes:
        outfile : file open in write mode\t
    """

    def __init__(self, outfile=sys.stdout):
        if type(outfile) is not file:
            raise Error('outfile argument must be a file')
        if 'w' in outfile.mode == False:
            raise Error('outfile argument must be a writing file')
        self.encoding = 'utf-8'
        self.out = outfile
        self.stackElement = []
        self.isAutoFormatting = True
        self.indent = '\t'
        self.isEmptyElement = False
        self.isOpenElement = False
        self.isData = False

    def write_start_document(self):
        """Create first xml markup"""
        self.out.write('<?xml version="1.0" encoding="')
        self.out.write(self.encoding + '" ?>')
        self.out.write('\n')

    def write_end_document(self):
        """End all open element"""
        while len(self.stackElement) > 0:
            self.write_end_element()
        self.out.flush()

    def write_start_element(self, name):
        """Open new element
        Add it at the last position to the open stack element"""
        self.__close_start_element(True)
        self.isEmptyElement = False
        self.isOpenElement = True
        self.isData = False
        self.stackElement.append(name)
        self.__indent_element()
        self.out.write('<' + name)

    def write_start_empty_element(self, name):
        """Open new auto-closed element
        Only attributes could be added, no new child or character
        """
        self.__close_start_element(True)
        self.isEmptyElement = True
        self.isOpenElement = True
        self.isData = False
        self.__indent_element(1)
        self.out.write('<' + name)

    def write_attribute(self, name, value):
        """Add attributes to current open element
        Raise error, if no element is open
        """
        if self.isOpenElement == False:
            raise Error('No open element to write attribut')
        self.out.write(' ' + name + '="' + value.encode(self.encoding) + '"')

    def write_character(self, character):
        """Add character to current element
        This closed automatically current element
        No indentation and return are add to closed element
        """
        if self.isOpenElement == False:
            raise Error('No open element to write character')
        if self.isEmptyElement:
            raise Error('Auto close element not allowed to write character')
        self.__close_start_element(False)
        self.out.write(character.encode(self.encoding))
        self.isData = True

    def write_cdata(self, cData):
        """Add CData to current element
        This closed automatically current element
        No indentation and return are add to closed element
        """
        if self.isOpenElement == False:
            raise Error('No open element to write CData')
        if self.isEmptyElement:
            raise Error('Auto close element not allowed to write CData')
        self.__close_start_element(False)
        self.out.write('<![CDATA["')
        self.out.write(cData.encode(self.encoding))
        self.out.write('"]]>')
        self.isData = True

    def write_end_element(self):
        """Closed the last element on the open stack element
        Add indentation if no character or CData have been add
        """
        if len(self.stackElement) == 0:
            raise Error('No current element to closed')
        self.__close_start_element(True)
        if self.isData == False:
            self.__indent_element()
        name = self.stackElement.pop()
        self.out.write('</' + name + '>' + '\n')
        self.isData = False

    def set_auto_formatting(self, enable=True):
        """Defined if indentation must be add automatically
        Must be defined before starting document.
        """
        self.isAutoFormatting = enable

    def set_auto_formatting_indent(self, spacesOrTabs='\t'):
        """Defined character use for indentation:
        Use multiple space or tab, default:Tab
        Must be defined before starting document.
        """
        self.indent = spacesOrTabs

    def set_codec(self, codec):
        """Defined current codec to applied on character or attributes values
        Must be defined before starting document.
        """
        self.encoding = codec

    def __indent_element(self, addSupIntent=0):
        """Private function to add indentation
        Depends on the stack open element
        add supplemental indentation as argument for auto-closed element
        """
        if self.isAutoFormatting == False:
            return None
        num = (len(self.stackElement) - 1) + addSupIntent
        while num > 0:
            self.out.write(self.indent)
            num -= 1

    def __close_start_element(self, addReturn):
        """Private function to terminate current open element"""
        if self.isOpenElement == False:
            return None
        if self.isEmptyElement:
            self.out.write(' />')
        else:
            self.out.write('>')
        self.isOpenElement = False
        if addReturn:
            self.out.write('\n')


if __name__ == '__main__':
    writer = Writer()
    writer.write_start_document()
    writer.write_start_element('xmlstream')
    writer.write_start_element('writer')
    writer.write_attribute('version', '0.1')
    writer.write_start_element('author')
    writer.write_character('Benoit Valot')
    writer.write_end_document()
