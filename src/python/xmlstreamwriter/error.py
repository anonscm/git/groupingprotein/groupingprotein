class Error(Exception):
    """Generic error during XML stream write
    Attributes:
        msg  -- explanation of the error
    """

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return (self.msg)
