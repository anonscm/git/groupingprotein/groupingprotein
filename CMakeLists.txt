
# cmake helps to compile "groupingprotein"
# please install the cmake package depending on your linux distribution
# and type in the beads root directory :
# cmake .
# make
# make install
cmake_minimum_required(VERSION 2.6)

# The name of this project is "gpf". CMakeLists files in this project can
# refer to the root source directory of the project as ${gpf_SOURCE_DIR}
# and to the root binary directory of the project as ${gpf_BINARY_DIR}.

PROJECT(groupingprotein CXX C)


if (CMAKE_VERSION VERSION_LESS "3.1")
    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      set (CMAKE_CXX_FLAGS "-std=gnu++11 ${CMAKE_CXX_FLAGS}")
    endif ()
else ()
    set (CMAKE_CXX_STANDARD 11)
endif ()


#depending on libpappsomspp version :
SET(LIBPAPPSOMSPP_VERSION "0.6.5-1")
configure_file (${CMAKE_SOURCE_DIR}/debian/control.cmake ${CMAKE_SOURCE_DIR}/debian/control  @ONLY)

# Project version

SET (GP_MAJOR_VERSION 0)
SET (GP_MINOR_VERSION 5)
SET (GP_PATCH_LEVEL   2)
SET (GP_VERSION  "${GP_MAJOR_VERSION}.${GP_MINOR_VERSION}.${GP_PATCH_LEVEL}")
#SET (GPF_SCHEMA_VERSION "${GPF_MAJOR_VERSION}.${GPF_MINOR_VERSION}")
#SET (GPF_SCHEMA_FILE "gpf-1.0.xsd")
#SET (GPF_XSD_LOCATION_DIR "${gpf_SOURCE_DIR}/doc/schema")

SET (GP_BINARY_DIR "bin")

IF (CMAKE_BASE_NAME MATCHES "cl")
  SET( WIN32 "true")
ELSE (CMAKE_BASE_NAME MATCHES "cl")
  SET( WIN32 "false")
ENDIF(CMAKE_BASE_NAME MATCHES "cl")


if (NOT CMAKE_INSTALL_PREFIX)
  SET (CMAKE_INSTALL_PREFIX /usr/local)
endif (NOT CMAKE_INSTALL_PREFIX)

#SET(CMAKE_BUILD_TYPE "Release")
#SET(CMAKE_BUILD_TYPE "RelWithDebInfo")
if (NOT CMAKE_BUILD_TYPE)
  SET( CMAKE_BUILD_TYPE "Release")
endif (NOT CMAKE_BUILD_TYPE)

IF(CMAKE_BUILD_TYPE MATCHES "Release")
  MESSAGE("compiling as release version")
  ADD_DEFINITIONS("-DQT_NO_DEBUG_OUTPUT")
ENDIF( CMAKE_BUILD_TYPE MATCHES "Release" )

IF(CMAKE_BUILD_TYPE MATCHES "Debug")
  MESSAGE("compiling as debug version")
ENDIF( CMAKE_BUILD_TYPE MATCHES "Debug" )

IF(CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo")
  MESSAGE("compiling as release with debug info version")
ENDIF( CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo" )


SET(CMAKE_CXX_FLAGS_DEBUG "-O3 -g -Wall")
SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O3 -g -Wall")
SET(CMAKE_CXX_FLAGS_RELEASE  "-s ${CMAKE_CXX_FLAGS_RELEASE}")
#SET(GPF_XSD_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/share/xml/schema")
#SET(GPF_XSD "${GPF_XSD_INSTALL_DIR}/${GPF_SCHEMA_FILE}")


SET(CPACK_CMAKE_GENERATOR "Unix Makefiles")
SET(CPACK_GENERATOR "STGZ;TGZ;TZ")
SET(CPACK_OUTPUT_CONFIG_FILE "./CPackConfig.cmake")
#SET(CPACK_PACKAGE_DESCRIPTION_FILE ${GPF_SOURCE_DIR}/COPYING)
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Grouping Protein algorithm")
SET(CPACK_PACKAGE_EXECUTABLES "groupingprotein")
SET(CPACK_SOURCE_PACKAGE_FILE_NAME "groupingprotein-${GP_VERSION}")
SET(CPACK_SYSTEM_NAME "Linux-i686")
SET(CPACK_PACKAGE_FILE_NAME "groupingprotein-${GP_VERSION}-${CPACK_SYSTEM_NAME}")
SET(CPACK_PACKAGE_INSTALL_DIRECTORY "groupingprotein ${GP_VERSION}")
SET(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "groupingprotein ${GP_VERSION}")
SET(CPACK_PACKAGE_NAME "groupingprotein")
SET(CPACK_PACKAGE_VENDOR "PAPPSO")
SET(CPACK_PACKAGE_VERSION ${GP_VERSION})
#SET(CPACK_RESOURCE_FILE_LICENSE ${GPF_SOURCE_DIR}/debian/copyright)
#SET(CPACK_RESOURCE_FILE_README ${GPF_SOURCE_DIR}/README)
#SET(CPACK_RESOURCE_FILE_WELCOME ${GPF_SOURCE_DIR}/README)
SET(CPACK_SOURCE_GENERATOR "TGZ;TZ")
SET(CPACK_SOURCE_OUTPUT_CONFIG_FILE "./CPackSourceConfig.cmake")
SET(CPACK_SOURCE_STRIP_FILES "")
SET(CPACK_SYSTEM_NAME "Linux-i686")
SET(CPACK_TOPLEVEL_TAG "Linux-i686")
SET(CPACK_SOURCE_PACKAGE_FILE_NAME "grouping-protein-${GP_VERSION}")


SET(CPACK_SOURCE_IGNORE_FILES
  ".*\\\\.tar\\\\.gz"
  "moc_.*cxx"
  ".*\\\\.pyc"
  "\\\\.#.*"
  ".*\\\\.*~"
  ".*\\\\.aux"
  ".*\\\\.bbl"
  ".*\\\\.blg"
  ".*\\\\.log"
  ".*\\\\.out"
  ".*\\\\.toc"
  "/devel_archives/"
  "/doc\\\\/misc/"
  "/doc\\\\/scripts/"
  "/doc\\\\/slides/"
   "/figures/"
  "/Soumis/"
  "Makefile"
  "install_manifest.txt"
  "CMakeCache.txt"
  "CPackConfig.cmake"
  "CPackSourceConfig.cmake"
  "/CMakeFiles/"
  "/_CPack_Packages/"
  "/Debug/"
  "/Release/"
  "/tests/"
  "/\\\\.externalToolBuilders/"
  "/\\\\.git/"
  "/\\\\.settings/"
  "Makefile"
  "\\\\.cdtbuild"
  "\\\\.cdtproject"
  "\\\\.project"
  "\\\\.cproject"
  "/win32/"
  "svnignores.txt"
  "\\\\.gitignore"
  "/build/"
  )
# to create a TGZ archive of the  source code type shell command
#

#dch -Dwheezy "message"
#cmake ..
#make deb
# scp grouping-protein* proteus.moulon.inra.fr:/var/www/apt/incoming
# reprepro -Vb /var/www/apt processincoming default
#
#  debuild -S -sa
# dput -f olivier-langella *changes


SET(CPACK_PACKAGE_EXECUTABLES "groupingprotein" "groupingprotein")



INCLUDE(CPack)

# Recurse into the "src" subdirectories.  This does not actually
# cause another cmake executable to run.  The same process will walk through
# the project's entire directory structure.
add_subdirectory (src)



add_custom_target(targz
	cpack -G TGZ --config CPackSourceConfig.cmake && tar xvfz grouping-protein-${GP_VERSION}.tar.gz
	WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
	COMMENT "Creating .tar.gz" VERBATIM
)


add_custom_target(deb
	cd grouping-protein-${GP_VERSION} && dpkg-buildpackage
	DEPENDS targz
	WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
	COMMENT "Creating Debian package" VERBATIM
)

file(GLOB changesdebs "${CMAKE_BINARY_DIR}/grouping-protein_*.dsc")

foreach(libfile ${changesdebs})
    SET(changesdeb "${libfile}")
endforeach(libfile)

message ("changes debian file : ${changesdeb}")
#lintian -IEi --pedantic tandem-mass_2013.06.15-1_amd64.changes
add_custom_target(lintian
	lintian -IEi --pedantic ${changesdeb}
	DEPENDS deb
	WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
	COMMENT "lintian check" VERBATIM
)
